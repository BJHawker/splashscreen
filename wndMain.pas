unit wndMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TfrmMain = class(TForm)
    imgMain: TImage;
    lblWait: TLabel;
  private
    procedure DisplayMedia(_MediaFileName : String = '');
  public
    Constructor Create(_Screen : Integer); overload;
  end;

var
  frmMain: TfrmMain;

implementation

uses uCommon;

{$R *.dfm}

{ TfrmMain }

constructor TfrmMain.Create(_Screen: Integer);
begin
  inherited Create(nil);
  Left := Screen.Monitors[_Screen].Left;
  Top := Screen.Monitors[_Screen].Top;
  Width := Screen.Monitors[_Screen].Width;
  Height := Screen.Monitors[_Screen].Height;
  lblWait.Left := 0;
  lblWait.Top := (Height-lblWait.Height) div 2;
  lblWait.Width := Width;
  DisplayMedia(AddSlash(ExtractFilePath(Application.ExeName))+'splash.png');
  Cursor := crNone;
end;

procedure TfrmMain.DisplayMedia(_MediaFileName : String = '');
var
  Pict:TPicture;
  lLoaded : Boolean;
begin

  lLoaded := False;
  if FileExists(_MediaFileName) then
  begin
    Pict:=TPicture.Create;
    Try
      Pict.LoadFromFile(_MediaFileName);
      lLoaded := True;
    Except
    End;
    if lLoaded then
    begin
      imgMain.Picture:=Pict;
      Stretch(imgMain, 0, 0, Width, Height);
    end;
    Pict.Free();
  end;
  lblWait.Visible := not lLoaded;
end;

end.
