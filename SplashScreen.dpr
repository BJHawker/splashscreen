program SplashScreen;

uses
  SysUtils,
  Forms,
  GraphicEx in 'GraphicEx\GraphicEx.pas',
  wndMain in 'wndMain.pas' {frmMain};

{$R *.res}
var
  nCount : Integer;

begin
  Application.Initialize;
  for nCount := 0 to Screen.MonitorCount-1 do TfrmMain.Create(nCount).Show;
  Application.Run;
  Application.ProcessMessages();
  Sleep(5000);
end.
