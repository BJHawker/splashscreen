object frmMain: TfrmMain
  Left = 841
  Top = 632
  BorderStyle = bsNone
  Caption = 'frmMain'
  ClientHeight = 474
  ClientWidth = 542
  Color = clBlack
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object imgMain: TImage
    Left = 0
    Top = 0
    Width = 542
    Height = 474
    Align = alClient
    Stretch = True
  end
  object lblWait: TLabel
    Left = 48
    Top = 176
    Width = 1683
    Height = 97
    Alignment = taCenter
    Caption = 'Veuillez patienter, chargement en cours...'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -80
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
end
